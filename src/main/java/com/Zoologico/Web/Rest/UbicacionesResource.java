package com.Zoologico.Web.Rest;


import com.Zoologico.Service.IUbicacionesService;
import com.Zoologico.domain.Ubicaciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UbicacionesResource {

    @Autowired
    IUbicacionesService ubicacionesService;

    @PostMapping("/ubicacion")
    public Ubicaciones create(@RequestBody Ubicaciones ubicaciones){
        return ubicacionesService.create(ubicaciones);
    }

    @GetMapping("/ubicacion")
    public Iterable<Ubicaciones> read(){
        return ubicacionesService.read();
    }

    @PutMapping("/ubicacion")
    public Ubicaciones update(@RequestBody Ubicaciones ubicaciones){
        return ubicacionesService.update(ubicaciones);
    }
}
