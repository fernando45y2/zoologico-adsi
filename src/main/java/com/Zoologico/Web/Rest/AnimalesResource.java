package com.Zoologico.Web.Rest;

import com.Zoologico.Service.IAnimalesService;
import com.Zoologico.domain.Animales;
import com.Zoologico.domain.enumeration.Genero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AnimalesResource {

    @Autowired
    IAnimalesService animalesService;

    @PostMapping("/animales")
    public Animales create(@RequestBody Animales animales){
        return animalesService.create(animales);
    }

    @GetMapping("/animales")
    public Iterable<Animales> read(){
        return animalesService.read();
    }

    @PutMapping("/animales")
    public Animales update(@RequestBody Animales animales){
        return  animalesService.update(animales);
    }

    @GetMapping("/animales/count")
    public Integer countAllAnimales(){
        return animalesService.countAllAnimales();
    }

    @GetMapping("/animales/get-genero-query/{genero}")
    public Iterable<Animales> getAllGeneroQuery(@PathVariable Genero genero){
        return animalesService.getAllAnimalesGeneroQuery(genero);
    }

    @GetMapping("/animales/get-genero/{genero}")
    public Iterable<Animales> getAllGeneroContrain(@PathVariable Genero genero){
        return animalesService.getAllAnimalesGeneroContrain(genero);
    }

    @GetMapping("/animales/genero/count")
    public Integer countAllAnimalesGenero(){
        return animalesService.countAllAnimalesGenero();
    }

}
