package com.Zoologico.Repository;

import com.Zoologico.domain.Ubicaciones;
import org.springframework.data.repository.CrudRepository;

public interface UbicacionRepository extends CrudRepository<Ubicaciones, Integer> {
}
