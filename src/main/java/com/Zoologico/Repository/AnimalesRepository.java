package com.Zoologico.Repository;

import com.Zoologico.domain.Animales;
import com.Zoologico.domain.enumeration.Genero;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AnimalesRepository extends CrudRepository<Animales, Integer> {

    @Query(value = "select count(animales) from Animales animales")
    Integer countAllAnimales();

    @Query(value = "select animales.genero as genero from Animales animales where animales.genero like %:genero%  ")
    Iterable<Animales> findAllByGeneroQuery(@Param("genero")Genero genero);

    Iterable<Animales> findAllByGeneroContains(Genero genero);

    @Query(value = "select count(genero) from Animales animales")
    Integer countAllAnimalesGenero();
}
