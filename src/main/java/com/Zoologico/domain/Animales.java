package com.Zoologico.domain;

import com.Zoologico.domain.enumeration.Genero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Animales {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")


    private int codigo;
    private String nombre;
    private String raza;
    private Genero genero;

   //Ubicacion
    @ManyToMany
    @JoinTable(name = "animales_has_ubicaciones",
                joinColumns = @JoinColumn(name = "id_animales"),
                inverseJoinColumns = @JoinColumn(name = "id_ubicaciones"))
    private Set<Ubicaciones> ubicaciones;


}
