package com.Zoologico.domain.enumeration;

public enum  TypeUbicacion {

    JAULA,
    LAGO,
    ACUARIO
}
