package com.Zoologico.Service;

import com.Zoologico.domain.Animales;
import com.Zoologico.domain.enumeration.Genero;

public interface IAnimalesService {

    public Animales create(Animales animales);
    public Iterable<Animales> read();
    public Animales update(Animales animales);

    public Integer countAllAnimales();
    public Iterable<Animales> getAllAnimalesGeneroQuery(Genero genero);
    public Iterable<Animales> getAllAnimalesGeneroContrain(Genero genero);
    public Integer countAllAnimalesGenero();

}
