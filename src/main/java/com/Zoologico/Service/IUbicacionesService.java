package com.Zoologico.Service;

import com.Zoologico.domain.Ubicaciones;

public interface IUbicacionesService {

    public Ubicaciones create(Ubicaciones ubicaciones);
    public Iterable<Ubicaciones> read();
    public Ubicaciones update(Ubicaciones ubicaciones);
}
