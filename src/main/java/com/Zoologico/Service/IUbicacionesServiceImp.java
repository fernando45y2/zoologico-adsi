package com.Zoologico.Service;

import com.Zoologico.Repository.UbicacionRepository;
import com.Zoologico.domain.Ubicaciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IUbicacionesServiceImp implements IUbicacionesService {

    @Autowired
    UbicacionRepository ubicacionRepository;

    @Override
    public Ubicaciones create(Ubicaciones ubicaciones) {
        return ubicacionRepository.save(ubicaciones);
    }

    @Override
    public Iterable<Ubicaciones> read() {
        return ubicacionRepository.findAll();
    }

    @Override
    public Ubicaciones update(Ubicaciones ubicaciones) {
        return ubicacionRepository.save(ubicaciones);
    }
}
