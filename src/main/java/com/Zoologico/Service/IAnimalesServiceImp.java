package com.Zoologico.Service;

import com.Zoologico.Repository.AnimalesRepository;
import com.Zoologico.domain.Animales;
import com.Zoologico.domain.enumeration.Genero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IAnimalesServiceImp implements IAnimalesService {

    @Autowired
    AnimalesRepository animalesRepository;

    @Override
    public Animales create(Animales animales) {
        return animalesRepository.save(animales);
    }

    @Override
    public Iterable<Animales> read() {
        return animalesRepository.findAll();
    }

    @Override
    public Animales update(Animales animales) {
        return animalesRepository.save(animales);
    }

    @Override
    public Integer countAllAnimales() {
        return animalesRepository.countAllAnimales();
    }

    @Override
    public Iterable<Animales> getAllAnimalesGeneroQuery(Genero genero) {
        return animalesRepository.findAllByGeneroQuery(genero);
    }

    @Override
    public Iterable<Animales> getAllAnimalesGeneroContrain(Genero genero) {
        return animalesRepository.findAllByGeneroContains(genero);
    }

    @Override
    public Integer countAllAnimalesGenero() {
        return animalesRepository.countAllAnimalesGenero();
    }
}
